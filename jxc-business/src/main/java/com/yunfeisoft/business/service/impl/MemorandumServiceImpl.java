package com.yunfeisoft.business.service.impl;

import com.applet.base.BaseServiceImpl;
import com.applet.sql.separation.DataSourceChange;
import com.applet.utils.Page;
import com.yunfeisoft.business.dao.inter.MemorandumDao;
import com.yunfeisoft.business.model.Memorandum;
import com.yunfeisoft.business.service.inter.MemorandumService;
import com.yunfeisoft.dao.inter.DataDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * ClassName: MemorandumServiceImpl
 * Description: 备忘录信息service实现
 * Author: Jackie liu
 * Date: 2020-08-23
 */
@Service("memorandumService")
public class MemorandumServiceImpl extends BaseServiceImpl<Memorandum, String, MemorandumDao> implements MemorandumService {

    @Autowired
    private DataDao dataDao;

    @Override
    @DataSourceChange(slave = true)
    public Page<Memorandum> queryPage(Map<String, Object> params) {
        return getDao().queryPage(params);
    }

    @Override
    public int save(Memorandum memorandum) {
        int result = super.save(memorandum);
        dataDao.saveOrUpdate(memorandum.getId(), memorandum.getContent());
        return result;
    }

    @Override
    public int modify(Memorandum memorandum) {
        int result = super.modify(memorandum);
        dataDao.saveOrUpdate(memorandum.getId(), memorandum.getContent());
        return result;
    }
}