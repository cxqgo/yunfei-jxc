package com.yunfeisoft.business.dao.inter;

import com.applet.base.BaseDao;
import com.yunfeisoft.business.model.Customer;
import com.applet.utils.Page;

import java.util.List;
import java.util.Map;

/**
 * ClassName: CustomerDao
 * Description: 客户信息Dao
 * Author: Jackie liu
 * Date: 2020-07-23
 */
public interface CustomerDao extends BaseDao<Customer, String> {

    public Page<Customer> queryPage(Map<String, Object> params);

    public List<Customer> queryList(Map<String, Object> params);

    public boolean isDupName(String orgId, String id, String name);
}