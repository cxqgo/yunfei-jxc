package com.yunfeisoft.business.dao.impl.postgres;

import com.applet.base.ServiceDaoImpl;
import com.applet.sql.builder.SelectBuilder;
import com.applet.sql.builder.WhereBuilder;
import com.applet.utils.DateUtils;
import com.applet.utils.Page;
import com.yunfeisoft.business.dao.inter.SaleItemDao;
import com.yunfeisoft.business.model.Customer;
import com.yunfeisoft.business.model.Product;
import com.yunfeisoft.business.model.SaleItem;
import com.yunfeisoft.business.model.SaleOrder;
import com.yunfeisoft.model.User;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * ClassName: SaleItemDaoImpl
 * Description: 销售单商品信息Dao实现
 * Author: Jackie liu
 * Date: 2020-07-23
 */
@Repository
public class SaleItemDaoImpl extends ServiceDaoImpl<SaleItem, String> implements SaleItemDao {

    @Override
    public Page<SaleItem> queryPage(Map<String, Object> params) {
        WhereBuilder wb = new WhereBuilder();
        wb.setOrderByWithDesc("so.saleDate");
        if (params != null) {
            initPageParam(wb, params);
            wb.andEquals("si.orgId", params.get("orgId"));
            wb.andEquals("p.id", params.get("productId"));
            wb.andEquals("so.status", params.get("orderStatus"));
            wb.andGreaterEquals("so.saleDate", DateUtils.getTimestamp(params.get("beginDate")));
            wb.andLessEquals("so.saleDate", DateUtils.getTimestamp(params.get("endDate")));
            wb.andFullLike("p.name", params.get("productName"));
            wb.andFullLike("u.name", params.get("createName"));
            wb.andFullLike("c.name", params.get("customerName"));
        }

        SelectBuilder builder = getSelectBuilder("si");
        builder.column("p.code as productCode")
                .column("p.name as productName")
                .column("p.standard as productStandard")
                .column("p.unit as productUnit")
                .column("so.code as orderCode")
                .column("so.saleDate as saleDate")
                .column("c.name as customerName")
                .column("u.name as createName")
                .join(Product.class).alias("p").on("si.productId = p.id").build()
                .join(SaleOrder.class).alias("so").on("si.saleOrderId = so.id").build()
                .join(Customer.class).alias("c").on("so.customerId = c.id").build()
                .join(User.class).alias("u").on("si.createId = u.id").build();

        return queryPage(builder.getSql(), wb);
    }

    @Override
    public List<SaleItem> queryBySaleOrderId(String saleOrderId) {
        if (StringUtils.isBlank(saleOrderId)) {
            return new ArrayList<>();
        }
        SelectBuilder builder = getSelectBuilder("si");
        builder.column("p.code as productCode")
                .column("p.name as productName")
                .column("p.standard as productStandard")
                .column("p.unit as productUnit")
                .join(Product.class).alias("p").on("si.productId = p.id").build();

        WhereBuilder wb = new WhereBuilder();
        wb.andEquals("si.saleOrderId", saleOrderId);
        return query(builder.getSql(), wb);
    }

    @Override
    public int removeBySaleOrderId(String saleOrderId) {
        if (StringUtils.isBlank(saleOrderId)) {
            return 0;
        }

        WhereBuilder wb = new WhereBuilder();
        wb.andEquals("saleOrderId", saleOrderId);
        return deleteByCondition(wb);
    }

}