package com.yunfeisoft.business.dao.inter;

import com.applet.base.BaseDao;
import com.yunfeisoft.business.model.Notice;
import com.applet.utils.Page;

import java.util.Map;

/**
 * ClassName: NoticeDao
 * Description: 公告信息Dao
 * Author: Jackie liu
 * Date: 2020-08-23
 */
public interface NoticeDao extends BaseDao<Notice, String> {

    public Page<Notice> queryPage(Map<String, Object> params);
}