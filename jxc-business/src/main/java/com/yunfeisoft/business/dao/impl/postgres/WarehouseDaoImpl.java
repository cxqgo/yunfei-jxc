package com.yunfeisoft.business.dao.impl.postgres;

import com.applet.base.ServiceDaoImpl;
import com.applet.sql.builder.WhereBuilder;
import com.applet.utils.Page;
import com.yunfeisoft.business.dao.inter.WarehouseDao;
import com.yunfeisoft.business.model.Warehouse;
import com.yunfeisoft.enumeration.YesNoEnum;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * ClassName: WarehouseDaoImpl
 * Description: 仓库信息Dao实现
 * Author: Jackie liu
 * Date: 2020-08-04
 */
@Repository
public class WarehouseDaoImpl extends ServiceDaoImpl<Warehouse, String> implements WarehouseDao {

    @Override
    public Page<Warehouse> queryPage(Map<String, Object> params) {
        WhereBuilder wb = new WhereBuilder();
        wb.setOrderByWithAsc("createTime");
        if (params != null) {
            initPageParam(wb, params);
            wb.andEquals("orgId", params.get("orgId"));
            wb.andFullLike("name", params.get("name"));
        }
        return queryPage(wb);
    }

    @Override
    public List<Warehouse> queryList(Map<String, Object> params) {
        WhereBuilder wb = new WhereBuilder();
        if (params != null) {
            wb.andEquals("orgId", params.get("orgId"));
            wb.andFullLike("name", params.get("name"));

            String userId = (String) params.get("userId");
            if (StringUtils.isNotBlank(userId)) {
                wb.andCustomSQL("ID_ IN (SELECT WAREHOUSE_ID_ FROM TT_WAREHOUSE_USER WHERE USER_ID_ = ?)", new Object[]{userId});
            }
        }
        return query(wb);
    }

    @Override
    public int modifyDefault(String id, String orgId) {
        if (StringUtils.isBlank(id) || StringUtils.isBlank(orgId)) {
            return 0;
        }

        WhereBuilder wb = new WhereBuilder();
        wb.andEquals("orgId", orgId);
        wb.andNotEquals("id", id);

        Warehouse warehouse = new Warehouse();
        warehouse.setIsDefault(YesNoEnum.NO_CANCEL.getValue());
        updateByCondition(warehouse, wb);

        warehouse.setId(id);
        warehouse.setIsDefault(YesNoEnum.YES_ACCPET.getValue());
        return update(warehouse);
    }

    @Override
    public Warehouse loadDefault(String orgId) {
        WhereBuilder wb = new WhereBuilder();
        wb.andEquals("orgId", orgId);
        wb.andEquals("isDefault", YesNoEnum.YES_ACCPET.getValue());

        List<Warehouse> warehouseList = query(wb);
        return CollectionUtils.isEmpty(warehouseList) ? null : warehouseList.get(0);
    }

    @Override
    public boolean isDupName(String orgId, String id, String name) {
        WhereBuilder wb = new WhereBuilder();
        wb.andEquals("isDel", 2);
        wb.andEquals("orgId", orgId);
        wb.andEquals("name", name);
        wb.andNotEquals("id", id);
        return this.count(wb) > 0;
    }
}