package com.yunfeisoft.business.dao.impl.postgres;

import com.applet.base.ServiceDaoImpl;
import com.applet.sql.builder.WhereBuilder;
import com.applet.utils.Page;
import com.yunfeisoft.business.dao.inter.PaymentReceivedDao;
import com.yunfeisoft.business.model.PaymentReceived;
import org.springframework.stereotype.Repository;

import java.util.Map;

/**
 * ClassName: PaymentReceivedDaoImpl
 * Description: 收付款信息Dao实现
 * Author: Jackie liu
 * Date: 2020-08-11
 */
@Repository
public class PaymentReceivedDaoImpl extends ServiceDaoImpl<PaymentReceived, String> implements PaymentReceivedDao {

    @Override
    public Page<PaymentReceived> queryPage(Map<String, Object> params) {
        WhereBuilder wb = new WhereBuilder();
        if (params != null) {
            initPageParam(wb, params);
            wb.andEquals("orgId", params.get("orgId"));
            wb.andFullLike("name", params.get("name"));
        }
        return queryPage(wb);
    }
}