package com.yunfeisoft.business.dao.impl.postgres;

import com.applet.base.ServiceDaoImpl;
import com.applet.sql.builder.WhereBuilder;
import com.applet.utils.Page;
import com.yunfeisoft.business.dao.inter.CodeBuilderDao;
import com.yunfeisoft.business.model.CodeBuilder;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * ClassName: CodeBuilderDaoImpl
 * Description: 编码生成记录Dao实现
 * Author: Jackie liu
 * Date: 2020-07-23
 */
@Repository
public class CodeBuilderDaoImpl extends ServiceDaoImpl<CodeBuilder, String> implements CodeBuilderDao {

    @Override
    public Page<CodeBuilder> queryPage(Map<String, Object> params) {
        WhereBuilder wb = new WhereBuilder();
        if (params != null) {
            initPageParam(wb, params);
        }
        return queryPage(wb);
    }

    @Override
    public List<CodeBuilder> queryByName(String name, String orgId) {
        WhereBuilder wb = new WhereBuilder();
        wb.andEquals("name", name);
        wb.andEquals("orgId", orgId);
        return query(wb);
    }
}