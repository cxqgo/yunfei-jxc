package com.yunfeisoft.business.model;

import com.alibaba.excel.annotation.ExcelProperty;
import com.applet.base.ServiceModel;
import com.applet.sql.record.TransientField;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * ClassName: WarehouseProduct
 * Description: 仓库商品信息
 *
 * @Author: Jackie liu
 * Date: 2020-08-04
 */
@Entity
@Table(name = "TT_WAREHOUSE_PRODUCT")
public class WarehouseProduct extends ServiceModel implements Serializable {

    /**
     * Field serialVersionUID: 序列号
     */
    private static final long serialVersionUID = 1L;

    @ExcelProperty("商品编码")
    @TransientField
    private String productCode;
    @ExcelProperty("商品名称")
    @TransientField
    private String productName;
    @ExcelProperty("商品规格")
    @TransientField
    private String productStandard;
    @ExcelProperty("商品单位")
    @TransientField
    private String productUnit;
    @ExcelProperty("仓库")
    @TransientField
    private String warehouseName;

    /**
     * 商品id
     */
    @Column
    private String productId;

    /**
     * 仓库id
     */
    @Column
    private String warehouseId;

    /**
     * 初始库存数量
     */
    @ExcelProperty("初始库存")
    @Column
    private BigDecimal initStock;

    /**
     * 库存数量
     */
    @ExcelProperty("当前库存")
    @Column
    private BigDecimal stock;

    /**
     * 缺货下限
     */
    @Column
    private BigDecimal shortageLimit;

    /**
     * 积压上限
     */
    @Column
    private BigDecimal backlogLimit;

    /**
     * 进货价
     */
    @TransientField
    private BigDecimal buyPrice;

    /**
     * 零售价
     */
    @TransientField
    private BigDecimal salePrice;

    /**
     * 会员价
     */
    @TransientField
    private BigDecimal memberPrice;

    /**
     * 批发价
     */
    @TransientField
    private BigDecimal tradePrice;

    /**
     * 大于0 表示缺货
     *
     * @return
     */
    public BigDecimal getShortage() {
        return shortageLimit.subtract(stock);
    }

    /**
     * 大于0 表示积压
     *
     * @return
     */
    public BigDecimal getBacklog() {
        return stock.subtract(backlogLimit);
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getWarehouseId() {
        return warehouseId;
    }

    public void setWarehouseId(String warehouseId) {
        this.warehouseId = warehouseId;
    }

    public BigDecimal getStock() {
        return stock;
    }

    public void setStock(BigDecimal stock) {
        this.stock = stock;
    }

    public BigDecimal getShortageLimit() {
        return shortageLimit;
    }

    public void setShortageLimit(BigDecimal shortageLimit) {
        this.shortageLimit = shortageLimit;
    }

    public BigDecimal getBacklogLimit() {
        return backlogLimit;
    }

    public void setBacklogLimit(BigDecimal backlogLimit) {
        this.backlogLimit = backlogLimit;
    }

    public String getWarehouseName() {
        return warehouseName;
    }

    public void setWarehouseName(String warehouseName) {
        this.warehouseName = warehouseName;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductStandard() {
        return productStandard;
    }

    public void setProductStandard(String productStandard) {
        this.productStandard = productStandard;
    }

    public String getProductUnit() {
        return productUnit;
    }

    public void setProductUnit(String productUnit) {
        this.productUnit = productUnit;
    }

    public BigDecimal getBuyPrice() {
        return buyPrice;
    }

    public void setBuyPrice(BigDecimal buyPrice) {
        this.buyPrice = buyPrice;
    }

    public BigDecimal getSalePrice() {
        return salePrice;
    }

    public void setSalePrice(BigDecimal salePrice) {
        this.salePrice = salePrice;
    }

    public BigDecimal getMemberPrice() {
        return memberPrice;
    }

    public void setMemberPrice(BigDecimal memberPrice) {
        this.memberPrice = memberPrice;
    }

    public BigDecimal getTradePrice() {
        return tradePrice;
    }

    public void setTradePrice(BigDecimal tradePrice) {
        this.tradePrice = tradePrice;
    }

    public BigDecimal getInitStock() {
        return initStock;
    }

    public void setInitStock(BigDecimal initStock) {
        this.initStock = initStock;
    }
}