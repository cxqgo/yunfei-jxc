<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>查看销售单信息</title>
    <#include "/common/vue_resource.ftl">
    <style>
        body{background:#F2F2F2;padding:15px;}
        .footer span{font-size:12px;margin-right:15px;}
        @media print {
            .income-container, .btn-container, .layui-card-header{display:none;}
        }
    </style>
</head>
<body>
<div id="app" v-cloak>
    <div class="layui-card btn-container">
        <#--<div class="layui-card-header">收款信息</div>-->
        <div class="layui-card-body">
            <button type="button" class="layui-btn layui-btn-normal" @click="toPrint">打印</button>
            <@auth code='income_sale_order'><button type="button" class="layui-btn layui-btn-normal" v-if="record.payStatus != 2 && record.status == 2" @click="income">收款</button></@auth>
            <@auth code='saleOrder_update'><button type="button" class="layui-btn layui-btn-normal" v-if="record.status != 2" @click="modify">编辑</button></@auth>
            <@auth code='change_sale_order'><button type="button" class="layui-btn" v-if="record.status == 2" @click="changeOrder">改单</button></@auth>
            <@auth code='saleOrder_delete'><button type="button" class="layui-btn layui-btn-primary" v-if="record.status != 2" @click="remove">删单</button></@auth>
        </div>
    </div>

    <div class="layui-card">
        <div class="layui-card-header">基本信息</div>
        <div class="layui-card-body" style="padding-top:20px;">
            <div style="text-align:center;font-size:26px;margin-bottom:20px;">${(user.organization.name)!}销售单</div>
            <div style="text-align:right;font-weight:bold;">单号：{{record.code}}</div>
            <#--<div style="text-align:right;">制单日期：{{record.saleDate}}</div>-->
            <div class="layui-row" style="font-size:12px;">
                <div class="layui-col-md6">
                    客户：{{record.customer && record.customer.name}}
                </div>
                <div class="layui-col-md6" style="text-align:right;">
                    制单日期：{{record.saleDate}}
                </div>
            </div>
            <table class="layui-table" lay-even lay-skin1="nob" lay-size="sm" style="margin-top:20px;">
                <thead>
                <tr>
                    <th style="width:20px;">序号</th>
                    <th>编码</th>
                    <th>品名</th>
                    <th>规格</th>
                    <th>单位</th>
                    <th>数量</th>
                    <@auth code='saleOrder_column_price'>
                        <th>售价</th>
                        <th>金额</th>
                    </@auth>
                    <th>备注</th>
                </tr>
                </thead>
                <tbody>
                <tr v-for="(item, index) in record.saleItemList">
                    <td>{{1 + index}}</td>
                    <td>{{item.productCode}}</td>
                    <td>{{item.productName}}</td>
                    <td>{{item.productStandard}}</td>
                    <td>{{item.productUnit}}</td>
                    <td>{{item.quantity}}</td>
                    <@auth code='saleOrder_column_price'>
                        <td>￥{{item.salePrice}}</td>
                        <td>￥{{item.saleAmount}}</td>
                    </@auth>
                    <td>{{item.remark}}</td>
                </tr>
                <tr>
                    <td></td>
                    <td>合计</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td style="font-weight:bold;">{{record.totalNum}}</td>
                    <@auth code='saleOrder_column_price'>
                        <td></td>
                        <td style="font-weight:bold;">￥{{record.totalAmount}}</td>
                    </@auth>
                    <td></td>
                </tr>
                <#--<tr>
                    <td colspan="9" class="footer">
                        <span>客户：{{record.customer && record.customer.name}}</span>
                        <span>地址：{{record.customer && record.customer.address}}</span>
                        <span>电话：{{record.customer && (record.customer.phone || record.customer.standbyPhone)}}</span>
                        <span>联系人：{{record.customer && record.customer.linkman}}</span>
                    </td>
                </tr>-->
                </tbody>
            </table>
        </div>
    </div>

    <@auth code='saleOrder_column_price'>
        <div class="layui-card income-container">
            <div class="layui-card-header">收款信息</div>
            <div class="layui-card-body">
                <table class="layui-table" lay-even lay-skin1="nob" lay-size="sm">
                    <thead>
                    <tr>
                        <th style="width:20px;">序号</th>
                        <th>收款日期</th>
                        <th>应收金额</th>
                        <th>实收金额</th>
                        <th>未收金额</th>
                        <th>备注</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr v-for="(item, index) in record.incomeRecordList">
                        <td>{{1 + index}}</td>
                        <td>{{item.createTimeStr}}</td>
                        <td>￥{{item.receiveAmount}}</td>
                        <td>￥{{item.incomeAmount}}</td>
                        <td>￥{{item.outAmount}}</td>
                        <td>{{item.remark}}</td>
                    </tr>
                    <tr v-if="record.incomeRecordList && record.incomeRecordList.length <= 0">
                        <td colspan="6" class="text-center">没有更多数据了...</td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </@auth>

</div>
<script>
    var app = new Vue({
        el: '#app',
        data: {
            record : {},
        },
        mounted: function () {
            this.loadData();
        },
        methods: {
            loadData: function () {
                if (!'${params.id!}') {
                    return;
                }
                var that = this;
                $.http.post('${params.contextPath}/web/saleOrder/query.json', {id: '${params.id!}'}).then(function (data) {
                    if (!data.success) {
                        $.message(data.message);
                        return;
                    }
                    var record = data.data, totalNum = 0;
                    var saleItemList = record.saleItemList;
                    for (var i = 0; i < saleItemList.length; i ++) {
                        totalNum = CalculateFloat.floatAdd(totalNum, saleItemList[i].quantity || 0);
                    }
                    record.totalNum = totalNum;
                    that.record = record;
                });
            },
            changeOrder: function (index) {
                var url = "${params.contextPath!}/view/business/saleOrder/saleOrder_edit.htm?id=${params.id!}";
                location.href = url;
                /*var that = this;
                $.http.post("${params.contextPath}/web/saleOrder/changeOrder.json", {id: '${params.id!}'}).then(function (data) {
                    $.message(data.message);
                    if (!data.success) {
                        return;
                    }
                    var url = "${params.contextPath!}/view/business/saleOrder/saleOrder_detail.htm?id=${params.id!}";
                    location.href = url;
                });*/
            },
            income: function () {
                var url = "${params.contextPath!}/view/business/incomeRecord/incomeRecord_edit.htm?saleOrderId=${params.id!}";
                DialogManager.open({url: url, width: '60%', height: '100%', title: '编辑销售单收款'});
            },
            modify: function () {
                var url = "${params.contextPath!}/view/business/saleOrder/saleOrder_edit.htm?id=${params.id!}";
                location.href = url;
            },
            toPrint: function () {
                var url = "${params.contextPath!}/view/business/saleOrder/saleOrder_print.htm?id=${params.id!}";
                location.href = url;
            },
            remove:function (index) {//删除
                var that = this;
                $.http.post("${params.contextPath}/web/saleOrder/delete.json", {ids: "${params.id!}"}).then(function (data) {
                    $.message(data.message);
                    if (!data.success) {
                        return;
                    }
                    var alt = layer.alert(data.message || "操作成功", function () {
                        parent.app.loadData();
                        parent.layer.closeAll();
                        layer.close(alt);
                    });
                });
            },
        }
    });
</script>
</body>

</html>