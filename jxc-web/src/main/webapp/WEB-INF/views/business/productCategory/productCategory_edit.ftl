<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>编辑商品类别</title>
	<#include "/common/vue_resource.ftl">
	<script type="text/javascript" src="${params.contextPath}/js/Convert_Pinyin.js"></script>
</head>
<body>
<div id="app" v-cloak>
	<div class="ui-form">
		<form class="layui-form" @submit.prevent="submitForm()" method="post">
			<#--<div class="layui-form-item">
				<label class="layui-form-label">上级节点<span class="ui-request">*</span></label>
				<div class="layui-input-block">
					<input type="text" v-model="record.parentName" class="layui-input" readonly style="cursor:pointer;"/>
				</div>
			</div>-->
			<div class="layui-form-item">
				<label class="layui-form-label">名称<span class="ui-request">*</span></label>
				<div class="layui-input-block">
					<input type="text" v-model="name" class="layui-input"/>
				</div>
			</div>
			<div class="layui-form-item">
				<label class="layui-form-label">拼音简码<span class="ui-request">*</span></label>
				<div class="layui-input-block">
					<input type="text" v-model="record.pinyinCode" placeholder="请输入拼音简码" class="layui-input"/>
				</div>
			</div>

			<div class="layui-form-item">
				<div class="layui-input-block">
					<input type="submit" value="保存" class="layui-btn" />
				</div>
			</div>
		</form>
	</div>
</div>
<script>
	var app = new Vue({
		el: '#app',
		data: {
			showTypes: false,
			record: {
				id: '${params.id!}',
				/*parentId: '1',
				parentName: '顶级节点',*/
				name: '',
				pinyinCode: '',
			},
			name: ''
		},
		mounted: function () {
			this.init();
			this.loadData();
		},
		watch: {
			'name': function (newVal, oldVal) {
				this.record.name = newVal || "";
				if (!newVal) {
					this.record.pinyinCode = "";
					return false;
				}
				this.record.pinyinCode = pinyin.getCamelChars(newVal);
			}
		},
		methods: {
			init: function () {
				var that = this;
				/*laydate.render({elem: '#beginDate', type:'datetime', done:function (value) {
						that.record.beginDate = value;
					}});*/

			},
			loadData: function () {
				if (!'${params.id!}') {
					return;
				}
				var that = this;
				$.http.post('${params.contextPath}/web/productCategory/query.json', {id: '${params.id!}'}).then(function (data) {
					if (!data.success) {
						$.message(data.message);
						return;
					}
					var item = data.data;
					for (var key in that.record) {
						that.record[key] = item[key];
					}
					that.name = item.name || "";
				});
			},
			submitForm: function () {
				$.http.post('${params.contextPath}/web/productCategory/<#if (params.id)??>modify<#else>save</#if>.json', this.record).then(function (data) {
					if (!data.success) {
						$.message(data.message);
						return;
					}
					var alt = layer.alert(data.message || "操作成功", function () {
						parent.app.loadData();
						parent.layer.closeAll();
						layer.close(alt);
					});
				});
			},
		}
	});
</script>
</body>

</html>
